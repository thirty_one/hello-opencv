### opencv c++ 环境配置记录

#### 安装


```
brew install opencv
```

```
brew install cmake
```

mac 环境自带gcc，没有单独安装


#### 测试opencv

代码如下
```
 Mat image = imread(str);
 imshow("test_opencv111",image);
```

#### cmake
编写CMakeLists

```

# 项目名称 
project(Practice) 
# include opencv
find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
# 可执行文件名 Practice
add_executable(Practice main.cpp)
```


#### 编译&运行

```
cmake .

make 

./Practice 

```

![运行结果](./screenshots/test.png)


#### err 处理

`ignoring file /opt/homebrew/lib/libopencv_core.4.8.1.dylib, building for macOS-x86_64 but attempting to link with file built for macOS-arm64
Undefined symbols for architecture x86_64:`

需要在CMakeLists添加

```
set(CMAKE_OSX_ARCHITECTURES "arm64")
```
