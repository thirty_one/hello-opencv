#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
using namespace cv;
void ImageThreshold(String str) {
    Mat image = imread(str);
    std::cout << "show image";
    imshow("test_opencv",image);

    Mat bbox, rect;
    QRCodeDetector qr = QRCodeDetector();
    std::string data = qr.detectAndDecode(image, bbox, rect);

    if (data.length() > 0) {
        std::cout << data;
    } else {
         std::cout << "0000";
    }

    waitKey(0);
}
int main() {
//  如果报错，可能是路径问题。可以采用绝对路径。
//  libc++abi.dylib: terminating with uncaught exception of type cv::Exception: OpenCV(4.5.3)
    String str = "/Users/jiangm/Desktop/tempcode/test-opencv/image/qr.png";
    ImageThreshold(str);
    return 0;
}